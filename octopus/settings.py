# -*- coding: utf-8 -*-

import os, sys

here = lambda *x: os.path.join(os.path.dirname(os.path.dirname(__file__)), 'files', *x)

BOT_NAME = 'octopus'

SPIDER_MODULES = [
    'octopus.spiders'
]
NEWSPIDER_MODULE = 'octopus.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'octopus (+http://www.yourdomain.com)'

ITEM_PIPELINES = {
    #'octopus.pipelines.multimedia.IllustratorPipeline': 1,
    #'octopus.pipelines.NoSQL.CouchDBPipeline':          0,
    'octopus.pipelines.NoSQL.MongoDBPipeline':          1,
    #'octopus.pipelines.Shopping.ValidationPipeline':    1,
    'octopus.pipelines.Shopping.CleanupPipeline':       1,
    'octopus.pipelines.Shopping.EnrichPipeline':        1,
}

#MONGODB_URI = 'mongodb://mist:GFX7-VVME-HGJ4-MDSW-U7JH@mongodb.hive.maher-pur.pl:35290/octopus'
#MONGODB_URI = 'mongodb://mist:GFX7-VVME-HGJ4-MDSW-U7JH@ds047720.mongolab.com:47720/octopus'
#MONGODB_DATABASE           = 'octopus'

#MONGODB_URI = 'mongodb://worm:GFX7-VVME-HGJ4-MDSW-U7JH@ds035300.mongolab.com:35300/vortex'
#MONGODB_DATABASE           = 'vortex'

MONGO_MODE = ''
#MONGO_MODE = 'octopus'
#MONGO_MODE = 'eagle-eye'

if MONGO_MODE=='octopus':
    MONGODB_URI = 'mongodb://localhost/octopus'
    MONGODB_DATABASE           = 'octopus'
elif MONGO_MODE=='eagle-eye':
    MONGODB_URI = 'mongodb://localhost/eagle-eye'
    MONGODB_DATABASE           = 'eagle-eye'
else:
    MONGODB_URI = 'mongodb://mist:0f377f95486d4aa0bf6734b72c36a0d0@ds047720.mongolab.com:47720/octopus'

MONGODB_DEFAULT_COLLECTION = 'scrapy_item'
MONGODB_REMAP_COLLECTION   = '%s'

MONGODB_ADD_TIMESTAMP      = 'when'
MONGODB_HALT_ON_DUPLICATES = False

#COUCHDB_SERVER        = 'http://nosql-master:5984/'
#COUCHDB_DB            = 'offers'
#COUCHDB_UNIQ_KEY      = 'uid'
#COUCHDB_IGNORE_FIELDS = ['visit_id', 'visit_status']

IMAGES_STORE = here('media', 'shop')

IMAGES_EXPIRES = 7

IMAGES_THUMBS = {
    'small': (50, 50),
    'big': (270, 270),
}

WEBSERVICE_HOST = '0.0.0.0'
WEBSERVICE_PORT = '39520'
WEBSERVICE_RESOURCES_BASE = {
    'scrapy.contrib.webservice.crawler.CrawlerResource': 1,
    'scrapy.contrib.webservice.enginestatus.EngineStatusResource': 1,
    'scrapy.contrib.webservice.stats.StatsResource': 1,
}
