# -*- coding: utf-8 -*-

import re, scrapy, pymongo

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

#****************************************************************************************

DEVISEs = [
    '$','£',
    'MAD',
]

#****************************************************************************************

class NativeItem(scrapy.Item):
    when        = scrapy.Field(default=None)
    link        = scrapy.Field()

#****************************************************************************************

class NativePipeline(object):
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)
    
    def __init__(self, crawler):
        self.crawler = crawler

