# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class VendorProduct(NativeItem):
    link        = scrapy.Field()
    site        = scrapy.Field()
    uid         = scrapy.Field()

    vendor      = scrapy.Field(default={})

    title       = scrapy.Field()
    summary     = scrapy.Field(default="")
    description = scrapy.Field(default="")
    categories  = scrapy.Field(default=[])

    shipping    = scrapy.Field(default={})
    stock       = scrapy.Field(default={})
    pricing     = scrapy.Field(default={})

    media       = scrapy.Field(default={})

    #**********************************************************************************************************************

    def filter_image(self, state, entry):
        yield entry['path']

    #**********************************************************************************************************************

    def cleanup(self):
        raw = self['uid']

        self['uid'] = ''

        for c in raw:
            try:
                x = int(c)

                self['uid'] += c
            except:
                pass

        try:
            self['uid'] = int(self['uid'])
        except ValueError,ex:
            pass

        #*****************************************************************************************************************

        for key in self['pricing']:
            if len(self['pricing'][key]):
                if type(self['pricing'][key]) is not dict:
                    if self['pricing'][key][0] in DEVISEs:
                        self['pricing'][key] = dict(
                            devise = self['pricing'][key][0],
                            value  = float(self['pricing'][key][1:]),
                        )

        #*****************************************************************************************************************

        return self

    #######################################################################################################################

    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")

    #######################################################################################################################

    class Schema:
        collection = 'retail_article'
        unique_key = [
            ('site', pymongo.ASCENDING),
            ('uid',  pymongo.ASCENDING),
        ]
        mapping    = {
            'image_urls': None,
        }
